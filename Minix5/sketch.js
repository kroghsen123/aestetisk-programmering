let x = 0;
let y = 0;
let spacing = 20;

function setup() {
  createCanvas(400, 400);
  background(220);
  frameRate(5);
}

function draw() {
  let r = random(255);
  let g = random(255);
  let b = random(255);

  fill(r, g, b);
  stroke(5);
  circle(200, 200, 50);
  console.log(g);
   stroke(1);
  if (g < 127) {
    line(x, y, x + spacing, y + spacing);
  } else {
    line(x, y + spacing, x + spacing, y);
  }
  x = x + spacing;
  if (x > width) {
    x = 0;
    y = y + spacing;
  }
  for(r; r<127; r++){
  circle(x + spacing, y + spacing, random(20));
  }
}

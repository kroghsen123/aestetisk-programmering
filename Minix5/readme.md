## colorPrint

![](minix5.PNG)

https://kroghsen123.gitlab.io/aestetisk-programmering/Minix5/

I found 10print interesting because it was elegant, yet simple to understand and explain. When i started working on this weeks project, my mind was set on something along the lines of "The Game of Life", but I would quickly fall into a negative spiral, of thinking about all the things i wasn't capable of programming. So going back to the understandability of the 10print code, I started working on personalizing that instead.

- Random colors have been a theme througout some of my earlier projects, so i thought to myself "Why not somehow work that theme in?". My idea was, that the randomization of colors would somehow be the fundation of the program. I put a circle in the middle of my canvas, and mad it shuffle through random colors for a while. I found Daniel Shiffmans code for his 10print challenge, and let that run in the background. Now i just needed a connection. I wanted the float of the fill to be the deciding factor in the 10print formular, but couldn't really make it work. While i was testing different ways to do it, i came to another realization. I realized that if i operated on the entire fill, it would only be a matter of the shade of grey. Instead I dissected the fill, separating the red, green and blue. Now I had pieces to work with.

I then conneted the 10print to the greenscale of the circle, by letting the greenfloat decide whether it was a right or left tilt. This was done with a simple if/else statement. The circles are made by a for loop connected to the redscale.

# References
- Soon Winnie & Cox, Geoff, "Auto-generator", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 121-142
- Nick Montfort et al. “Randomness,” 10 PRINT CHR$(205.5+RND(1)); : GOTO10, https://10print.org/ (Cambridge, MA: MIT Press, 2012), 119-146.
- Daniel Shiffman, “p5.js - 2D Arrays in Javascript,” Youtube,https://www.youtube.com/watch?v=OTNpiLUSiB4.

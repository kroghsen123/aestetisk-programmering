# Minix 1

![](minix1.PNG)

https://kroghsen123.gitlab.io/aestetisk-programmering/Minix1/

### The product
The purpose of my fist project, was to create a hidden message. I spelled out HEY using rectangles and lines. The coordinates for the figures were given on the basis that the rectMode was corner. When changed to center, the squares made no sense. The premise was to have a button that changed the rectMode from center to corner, so that the message would appear.

### The process
From doing this I have learned that even the most simple task can have unforseen implications. I've had a lot of trouble trying to remove earlier code while making new code apppear. My thought process was, that i would simply ask it to change rectMode from center to corner, but instead it just appeared in a pile.

I am a beginner in the field of coding. When I try to read other peoples code, I am lost to say the least. When i started this project, I was just as lost. I started watching tutorials, and they quickly led me to standard rectangles. Forming several of these, would lead me to create a simple E out of the rectangles. This is where the idea came from. At the moment i gain nothing from reading code, but trying out simple things for myself, gives me room to explore.
(The program was fixed later)

### Differences and similarities to reading and writing
Writing and coding are both forms of literacy. Coding is the literacy of the modern era. The ability to code will open a whole new world of opportunities for a person, and give them a better understanding of the reality they live in. 

# References
- Soon Winnie & Cox, Geoff, "Getting started", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 27-48
- p5.js. p5.js | get started. Available at: https://p5js.org/get-started/ 
- p5.js. p5.js | references. Available at https://p5js.org/reference/
- Video: Lauren McCarthy, Learning While making P5.JS, OPENVIS Conference (2015).
- Video: Daniel Shiffman, Code! Programming with p5.js, The Coding Train. Available at https://www.youtube.com/watch?v=yPWkPOfnGsw&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=2
- Annette Vee, "Coding for Everyone and the Legacy of Mass Literacy", Coding Literacty: How Computer Programming Is Changing Writing Cambridge, Mass.: MIT Press, 2017), 43-93.

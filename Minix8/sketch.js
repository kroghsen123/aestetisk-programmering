let button1, button2, button3, button4;
let members;

function preload() {
 members = loadJSON('members.json');
}

function loadData(name) {
  console.log(name);
  textSize(20);
if(name == "Asger"){
background(0, 175, 100);
text("Name:", 100, 100);
text("Age:", 100, 150);
text("Gender:", 100, 200);
text("Interests:", 100, 250);
print(members.Members[0]);
text(members.Members[0].Name, 200, 100);
text(members.Members[0].Age, 200, 150);
text(members.Members[0].Gender, 200, 200);
text(members.Members[0].Interest, 200, 250);
}else  if(name == "Cecilie"){
background(193, 156, 156);
text("Name:", 100, 100);
text("Age:", 100, 150);
text("Gender:", 100, 200);
text("Interests:", 100, 250);
print(members.Members[1]);
text(members.Members[1].Name, 200, 100);
text(members.Members[1].Age, 200, 150);
text(members.Members[1].Gender, 200, 200);
text(members.Members[1].Interest, 200, 250);
}else  if(name == "Markus"){
background(125, 0, 125);
text("Name:", 100, 100);
text("Age:", 100, 150);
text("Gender:", 100, 200);
text("Interests:", 100, 250);
print(members.Members[2]);
text(members.Members[2].Name, 200, 100);
text(members.Members[2].Age, 200, 150);
text(members.Members[2].Gender, 200, 200);
text(members.Members[2].Interest, 200, 250);
}else  if(name == "Mathias"){
background(0, 220, 125);
text("Name:", 100, 100);
text("Age:", 100, 150);
text("Gender:", 100, 200);
text("Interests:", 100, 250);
print(members.Members[3]);
text(members.Members[3].Name, 200, 100);
text(members.Members[3].Age, 200, 150);
text(members.Members[3].Gender, 200, 200);
text(members.Members[3].Interest, 200, 250);
  }
}


function setup () {
  createCanvas(800, 600);
  background(0, 220, 125);
  textSize(20);
  text("Name:", 100, 100);
  text("Age:", 100, 150);
  text("Gender:", 100, 200);
  text("Interests:", 100, 250);
}

function draw() {
  button1 = createButton("Asger");
  button1.position(10,10);
  button1.mousePressed(function (){ loadData("Asger") });
  button2 = createButton("Cecilie");
  button2.position(10,40);
  button2.mousePressed(function (){ loadData("Cecilie") });
  button3 = createButton("Markus");
  button3.position(10,70);
  button3.mousePressed(function (){ loadData("Markus") });
  button4 = createButton("Mathias");
  button4.position(10,100);
  button4.mousePressed(function (){ loadData("Mathias") });
}

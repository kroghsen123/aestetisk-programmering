https://kroghsen123.gitlab.io/aestetisk-programmering/Minix6/
![](Whatever.PNG)

For some reason gitlab won't let me upload anything else than pictures tonight. I don't know what the issue is, but i will have to turn my things in tomorrow instead.

I planned to rework my second MiniX.

The idea came to me, when one of my study buddies mentioned that he was colorblind. I realized that the emojis colors, when randomized in a RGB scale, would be misrepresenting to a user that might be colorblind. Though the outcome of the emojis color would be the same, a colorblind sender of this emoji would think that the visual they sent the receiver, was different. To lower the risk of miscommunication, I wanted to include sliders for the different colors, so that the person producing the emojis would have control of the scales which in the emoji would be generated.

Working with this projecct that i developed early in my journey with programming, it was clear that a lot of the code was pretty basic. Given that the project was pretty basic, there was also a lot of room for improvement. First of all, I fixed the clunky problem of having to refresh the site, each time a new iteration was wanted. A simple button made it much easier and enjoyable, while also making the project interactable. Later I wanted to add the core change, the sliders, which turned out not working as intended. I tried taking inspiration from mi last Minix, by separating the colors and work with them as 3 separed variables.

## References 
- Soon Winnie & Cox, Geoff, "Preface", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 12-24
- Ratto, Matt & Hertz, Garnet, “Critical Making and Interdisciplinary Learning: Making as a Bridge between Art, Science, Engineering and Social Interventions” In Bogers, Loes, and Letizia Chiappini, eds. The Critical Makers Reader: (Un)Learning Technology. the Institute of Network Cultures, Amsterdam, 2019, pp. 17-28 (available free online: https://networkcultures.org/blog/publication/the-critical-makers-reader-unlearning-technology/)

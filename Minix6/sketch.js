function setup() {
//interactables
rSlider = createSlider(1, 255, 86);
gSlider = createSlider(1, 255, 86);
bSlider = createSlider(1, 255, 86);
rSlider.position(5, 360);
gSlider.position(5, 380);
bSlider.position(5, 400);
Button = createButton("Adjust");
Button.position(10, 335);
Button.mousePressed(resetSketch);

//Face canvas
createCanvas(400, 400);
  fill(random(250), random(250), random(250));
  ellipse(200,200,random(180,220),random(180,220));
// Mouth
  fill(240);
  arc(200, 250, 70, 60, -0.8, PI + QUARTER_PI, CHORD);
// Eyeballs
  ellipseMode(CENTER);
  fill(250,250,250);
  ellipse(random(150,170),random(160,180),random(47,52));
  ellipse(random(230,250),random(160,180),random(47,52));
//Eyecolor
  strokeWeight(5);
  stroke(random(250),random(250),random(250));
//Pupils
  ellipseMode(CENTER);
  fill(1,1,1);
  ellipse(240,170,18);
  ellipse(160,170,18);
//Teeth
  strokeWeight(1);
  stroke(0);
  rectMode(CORNER);
  fill(240,240,240);
  rect(180,227, random(15,20), random(15,20));
  rect(200,227, random(15,20), random(15,20));
}

function resetSketch() {
  r = rSlider.value;
  g = gSlider.value;
  b = bSlider.value;
//Face canvas
  createCanvas(400,400);
  fill(r, g, b);
  ellipse(200,200,random(180,220),random(180,220));
// Mouth
  fill(240);
  arc(200, 250, 70, 60, -0.8, PI + QUARTER_PI, CHORD);
// Eyeballs
  ellipseMode(CENTER);
  fill(250,250,250);
  ellipse(random(150,170),random(160,180),random(47,52));
  ellipse(random(230,250),random(160,180),random(47,52));
//Eyecolor
  strokeWeight(5);
  stroke(r, g, b);
//Pupils
  ellipseMode(CENTER);
  fill(1,1,1);
  ellipse(240,170,18);
  ellipse(160,170,18);
//Teeth
  strokeWeight(1);
  stroke(0);
  rectMode(CORNER);
  fill(240,240,240);
  rect(180,227, random(15,20), random(15,20));
  rect(200,227, random(15,20), random(15,20));
}

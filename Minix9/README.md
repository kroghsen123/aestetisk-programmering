# miniX9: Working with APIs

### *Find your soulmate*
| Frontpage   | Result page|
|-------------|------------|
| <img src="screenshot.jpg" width="400">  | <img src="screenshot2.jpg" width="400">|

Screenshot of our miniX9

## Links

[Sketch file](https://gitlab.com/kroghsen123/aestetisk-programmering/-/blob/master/Minix9/sketch.js)

[RunMe URL](https://kroghsen123.gitlab.io/aestetisk-programmering/Minix9/)

## What is the program about?
Linking the program to Faker API [[3]](https://fakerapi.it/en), we have tried to create a fake dating site, localizing your potential soulmate for you. The API generates pseudo random information about an imaginary user, making it seems as if it has actually connected you to another user, when in reality, you are the only user present. The only parameters working as deciding factors for the final result, are the wanted age and gender. The genders are limited to male and female, and the age is limited between 18 to 100. Keep this in mind as it will be referred to later. The "information" that you are supposedly supplied with are, first- and last name, email, phonenumber, birthday, gender, address and a picture.

The former description was the intended purpose of our program, yet due to unforeseen implications and a lack of time, the final product ended up with no address information. Otherwise there is nothing missing in relation to what we intended to do.

## Process and reflection
When searching for an API for this project, we ended up running into several problems, such as availability and gated data requiring processed information about the receiving party before it would become accessible. We intended to work with Twitter's API, but due to these reasons, we would have to wait approximately two weeks before we could receive anything from it.

After extensive research we saw potential in the Faker API. The process itself would give us full control of the data output, letting us relate to the potential thought process of an API owner, while at the same time working the subject, feeding them fake information presented as real data.

The Faker API turned out to be a little different from what we expected from it as an API, but we worked it out. In the API docs on the Faker API web page it is noted how to request data from the API through an URL. This is pretty standard and looks a lot like the example with Google's Image Search from the Aesthetic Programming book [[2]](https://gitlab.com/aesthetic-programming/book/-/tree/master/public/p5_SampleCode/ch8_Que(e)ryData) except for the fact that the API generates a new object(s) every time you refresh the page/send a new request with the same URL (https://fakerapi.it/api/v1/persons?_quantity=1&_gender=male&_birthday_start=2005-01-01). Here is an example of how the data is represented:

```js
{
    "status": "OK",
    "code": 200,
    "total": 1,
    "data": [
        {
            "firstname": "Darrick",
            "lastname": "Stiedemann",
            "email": "buckridge.tressie@muller.com",
            "phone": "+8814271144679",
            "birthday": "2013-10-24",
            "gender": "male",
            "address": {
                "street": "47770 Connie Junction",
                "streetName": "Casper Heights",
                "buildingNumber": "2460",
                "city": "Paucekburgh",
                "zipcode": "18876-2607",
                "country": "Vanuatu",
                "county_code": "LT",
                "latitude": 86.420219,
                "longitude": 51.925596
            },
            "website": "http://barrows.com",
            "image": "http://placeimg.com/640/480/people"
        }
    ]
}
```

The funny (and weird) thing is also, that the image-link (http://placeimg.com/640/480/people) is the same for every person object that will be generated, which essentially means that we did not need to use the `random()` function in our code. The most time consuming problem we encountered was due to CORS (Cross-Origin Resource Sharing) [6] issues. When trying to access the images through the Faker API by using the `loadJSON()` function it seemed as though the API's security blocked the request every time, which meant we could not access any of the data. In the attempt of trying to solve this problem it led us to do a comprehensive Google search and we wound up finding somewhat of an answer (or at least a fix) on Stack Overflow [[4]](https://stackoverflow.com/questions/35190615/api-gateway-cors-no-access-control-allow-origin-header) and on the Processing Foundation's website [[5]](https://discourse.processing.org/t/cors-issue-when-loading-json-in-p5/16727). We added the `"callback=getData"` at the end of the request-string and made some changes in the code (mainly the callback function).

It can be argued that the program has a queer element to it. As mentioned earlier, the possibility of choosing gender is very limited, which is a deliberate decision we made. The thought of the heteronormative view on gender might leave the user with a certain perception in the first place, only to realize later how minimal the impact of that specific decision. Whether you choose male or female, the randomness of the name, email and picture will not change. In a way this flips the script of the original connotations about the programs agenda, taking power away from a traditionally restrictive element. Same argument can be made for the age.

The API is presented as a tool to give a programmer the ability to spin up some quick and convincing fake data, but as the same time, we as programmers do also need to acknowledge, that we are oblivious to the origins of this data as well. It is not described to us what data is fed to the generator, and therefore we cannot adjust the data in order to combat potential -sism's or other forms discrimination.
This would certainly be an area to investigate further if we wanted- and had time to see this project more thoroughly through. Investigating the parameters deciding the data output, as well as Alessandro Pietrantonio (the creator) as a person, would help us localize socially cultural implications earlier in the process.

## Inspiration and references
##### Inspiration
* [[1] Public APIs (*Github repository by 'marekdano'*)](https://github.com/public-apis/public-apis)
* [[2] Sample code (path: book / public / p5_SampleCode / ch8_Que(e)ryData) from *Aesthetic Programming* book by Soon, Winnie & Cox, Geoff](https://gitlab.com/aesthetic-programming/book/-/tree/master/public/p5_SampleCode/ch8_Que(e)ryData)

##### References
* [[3] Faker API (*by Alessandro Pietrantonio*)](https://fakerapi.it/en)
* [[4] Stack Overflow question - API Gateway CORS: no 'Access-Control-Allow-Origin' Header](https://stackoverflow.com/questions/35190615/api-gateway-cors-no-access-control-allow-origin-header)
* [[5] Processing Foundation question - CORS issue when loading JSON in p5](https://discourse.processing.org/t/cors-issue-when-loading-json-in-p5/16727)
* [6] Soon Winnie & Cox, Geoff, "Que(e)ry data", *Aesthetic Programming: A Handbook of Software Studies*, London: Open Humanities Press, 2020, pp. 186-210.

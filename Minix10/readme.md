# miniX10: Flowcharts

## Groupwork: Group 10

## Abstraction in the face of complexity
One of the disadvantages of a very simplistic flowchart may be that some of the information is lost, but if they are too complex, they become unreadable to the common person. The best way to combat this complexity is to choose your target audience and stick to it, for our flowcharts the point was to give an idea of the flow that our two ideas would have, on a level where the actual program wasn't set in stone yet, meaning they had to be flexible. So in our case it isn't a problem, that the flowcharts don't show anything about the code level, as that isn't its job/function.


### make me non binary: simplicity warning

![The flowchart](https://i.imgur.com/nE4kRyc.png)

This program doesn't have any large technical challenges, which sounds like a good thing but... Due to the simplistic nature of the program, it has the danger of being to small in scope, and with that it also cant be scaled up with ease. Beyond that the flowcharts abstraction also creates a concern of the program being harder than assumed, which the simplicity of the flowchart could be a signifier for.

### Freedom of speech project: avoiding a yikes/10
![the flowchart2](https://i.imgur.com/1YOqkGa.png)

This project faces the technical problem of being largely technically undefined, and what seems like simple tasks in the flowchart like for example: "downloading and reading the data" could quickly balloon out of control. Beyond that the program faces a non technical issue, as we have to walk on metaphorical eggshells since the in order to show the issues we wanna bring light to, we plan on giving a platform to bigots, so if we don't do it responsibly, it could end up platforming ideas we don't want to platform. We also don't want persons who could find offense with the content to miss out on our larger point, which is why we have the little jumping off point before the program gets started.  


### flowcharts as tools
Flowcharts help to give an overall impression of what ideas and thoughts, we have made about what the final project should be like. These two flowcharts are made as preparation for our final program, whereas our individual flowcharts are a way to explain our previously made programs in smaller chunks.

> "By the early 1970s, the conventional wisdom was that “developing a program flowchart is a necessary first step in the preparation of a computer program." (Ensmenger, 2016, p. 323).

As Ensmenger describes in his text on flowcharts and their use, the initial idea was that they should be used as a planning tool for the development of computer programs.
In our experience, flowcharts that are made before a program is written often have a higher level of abstraction, as they are made to describe the "flow" of the program, more than the actual functionality. This might be the other way around for flowcharts that are made after a program is written, as it, for the most part, is a lot easier and straightforward to describe the in-depth functionality of the code, thus leading to a lower level of abstraction but a more accurate illustration of what the flow of commands within the program.

## Individual flowchart

![](page_1.png)

This is a flowchart made for my Minix8. The flowchart is made in Axure as it is a software that I have used before. The program functions as a short desription of group 10's members, with the user being able to pick who they want to read about. The descriptions are imported from a json file. All of the description are chosen by the represented people themselves as to not mislabel anyone, yet they are in the confines of the categories.

I have tried to work within the same level of abstraction as we did in our groupwork. The focus of the flowchart lies primarily on the what could be observed by a user, yet there is still a slight mentioning of the json file working behind. This is simply to show the user that they are searching in a datafile. 

Had I not known the inner workings of the program, the center part of the flowchart would have probably looked a lot different, though I feel that this gives the user a understanding of the program, without overcomplicating things.

##### References
* [1] Soon, Winnie & Cox, Geoff, "Algorithmic procedures", *Aesthetic Programming: A Handbook of Software Studies*, London: Open Humanities Press, 2020, pp. 211-226.
* [2] Ensmenger, Nathan, “The Multiple Meanings of a Flowchart,” Information & Culture: A Journal of History 51, no.3 (2016): 321-351, Project MUSE, doi:10.1353/lac.2016.0013.

https://kroghsen123.gitlab.io/aestetisk-programmering/Minix3/

![](minix3.PNG)

This project was only a small iteration of the code we got from Winnie. The goal was to create a sun with a button in the middle. The button was supposed to permanently change the background to yellow, and the throbber to black, representing the sun as the blackbody it really is.

My coding ability is very limited, and i will be the first to admit that this is not great work. I've had quite a tumultuous week, and it's been hard to find the time to really get into it. I hope to catch up with my fellow students during the next week, and to have a much better project to present.

Furthermore i noticed that the web editor doesn't translate perfectly into atom. The button and yellow ellipse were both centered in the throbber originally.
    (This was due to me not using width and height on these)

If you know how to make this project work, I would be more than happy to listen.
    (Update: Made it work later)

Wish you a great week :)

# References
- Soon Winnie & Cox, Geoff, "Infinite Loops", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 71-96
- Hans Lammerant, “How humans and machines negotiate experience of time,” in The Techno-Galactic Guide to Software Observation, 88-98, (2018), https://monoskop.org/log/?p=20190.
- Daniel Shiffman, Code! Programming with p5.js on YouTube, The Coding Train. [Video 3.1, 3.2, 3.3, 3.4, 4.1, 4.2, 5.1, 5.2, 5.3, 7.1, 7.2]

## Further references
- Soon Winnie: Aesthetic Programming: Class 3

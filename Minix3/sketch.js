function setup() {
 createCanvas(windowWidth, windowHeight);
 frameRate (10);
  button = createButton('SUN');
  button.position(width-335, height-245);
  button.mousePressed(changeBG);

  checkbox = createCheckbox('loop()', true);
  checkbox.changed(checkLoop);

}

function changeBG() {
  if (isLooping()) {
     background(255,255,0);
     let num = 8;
     push();
     translate(width/2, height/2);
     let cir = 360/num*(frameCount%num);
     rotate(radians(cir));
     noStroke();
     fill(0, 0, 0);
     rect(35, 0, 20, 20);
     triangle(30, 75, 58, 20, 86, 75);
     fill(0, 0, 0);
     ellipse(0, 0, 30, 30);
     pop();


  }
}

function checkLoop() {
  if (this.checked()) {
    loop();
  } else {
    noLoop();
  }
}

function draw() {
  background(1, 100);
  drawElements();
}

function drawElements() {
  let num = 8;
  push();
  translate(width/2, height/2);
  let cir = 360/num*(frameCount%num);
  rotate(radians(cir));
  noStroke();
  fill(255, 255, 0);
  rect(35, 0, 20, 20);
  triangle(30, 75, 58, 20, 86, 75);
  fill(255, 255, 0);
  ellipse(0, 0, 30, 30);
  pop();
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}

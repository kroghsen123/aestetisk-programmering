function setup() {
createCanvas(400,400);
  fill(random(250),random(250),random(250))
  ellipse(200,200,random(180,220),random(180,220))
  //Face canvas
  fill(240)
  arc(200, 250, 70, 60, -0.8, PI + QUARTER_PI, CHORD)
  //Mouth
    ellipseMode(CENTER)
  fill(250,250,250)
  ellipse(random(150,170),random(160,180),random(47,52));
  ellipse(random(230,250),random(160,180),random(47,52));
  //Eyeballs
  strokeWeight(5)
  stroke(random(250),random(250),random(250))
  //Eyecolor
  ellipseMode(CENTER)
  fill(1,1,1)
  ellipse(240,170,18)
  ellipse(160,170,18)
  //Pupils
  strokeWeight(1)
  stroke(0)
  rectMode(CORNER)
  fill(240,240,240)
  rect(180,227, random(15,20), random(15,20))
  rect(200,227, random(15,20), random(15,20))
  //Teeth
}

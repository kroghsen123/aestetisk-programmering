# Geauty
## (G)oogly B(eauty)

Hello

![](minix2.PNG)

https://kroghsen123.gitlab.io/aestetisk-programmering/Minix2/
1. In this program, I tried to implement a lot of randomization to the process of creating an emoji. Certain factors were preditermined either because i felt that it was fitting, necesary, or in one case because of my incompetance in coding. The preditermined factor were
    - The coordinates for the thing I call the face canvas.
    - The coordinates for the pupils.
    - Mouth
    - Color of the mouth.
    - Color of the pupils.
    - StrokeWeight.
    - The boundaries for the randomization.
Rest of the factors are, by the computer, randomized to a certain extent. Refresh the site in order to get a new emoji. 

2. The syntax for this project is primarily centered around the idea of random, due to the message i wanted to express with this code.

3. My emoji is a critique of the symmetrical nature of emojis. Faces are, as any other bodypart, rarely symmetrical and differing from one person to another. By randomizing the placement of different facial features, I include our different imperfections such as gap-teeth.  

# References 
- Soon Winnie & Cox, Geoff, "Variable Geometry", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 51-70
- Roel Roscam Abbing, Peggy Pierrot and Femke Snelting, "Modifying the Universal." Executing Practices, Eds. Helen Pritchard, Eric Snodgrass & Magda Tyżlik-Carver, London: Open Humanities Press, 2018, 35-51.
- p5.js. p5.js | Simple Shapes. Available at: https://p5js.org/examples/hello-p5-simple-shapes.html.
Daniel Shiffman, Code! Programming with p5.js, The Coding Train, Available at: https://www.youtube.com/watch?v=yPWkPOfnGsw&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=2 [video 1.3, 1.4, 2.1, 2.2]

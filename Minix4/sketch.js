let button;
let mic;
let frus = 0.17;

function setup() {
createCanvas(windowWidth, windowHeight);
//Red background with green circles in random sizes and with random lokations
background(200, 0, 0);
fill(0, 400, 0);
for(let i = 0; i<20; i++){
circle(random(windowWidth), random(windowHeight), random(1, 80));
}

button = createButton("Relief");
button.style("color","#fff");
button.style("background", "#4c69ba");
button.position(width/3, height/2);

mic = new p5.AudioIn();
mic.start();
}

function draw() {
  userStartAudio();
  let vol = mic.getLevel();
  console.log(vol);
  button.size(floor(map(vol, 0, 1, 40, 450)));

  fill(0,0,300);
  textSize(30);
  text("Shout For Relief",100,100);

  if (vol > frus) {
    text("You Are Relieved", 100, 400);
  } else {
    text("")
  }
}

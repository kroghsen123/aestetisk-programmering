![](minix4.PNG)

https://kroghsen123.gitlab.io/aestetisk-programmering/Minix4/

**Sound of Relief**

This project was produced at a time where I felt a certain amount of frustration, so please keep that in mind.

As a person who is already quite skeptical of the current state of technology, one piece of information from the former weeks learning material rubbed me the wrong way. In the documentary about surveilance capitalism featuring Shoshana Zuboff (https://www.youtube.com/watch?v=hIXhnWUmMvw), she talks about how Facebook have been experimenting with their ability to alter young peoples emotions, in the pursuit of monetary gains. To me, it seems as an unhealthy and very scary influence to hold. As a person born in a digital era, I know that convinsing people to let go of their social medias, is a very hard thing to do. Instead I wanted to offer people an alternative outlet for their frustations. An outlet that would not log the emotional letout and use it against people. 

The project that I have presented for this week is, at it's core, just crutches for a more primal outlet. The program is simply encouraging the user to shout. As and ekstra encouraging factor, the bar for relief will increase with the users volume. When the volume reaches a certain level, the program will inform the user that they are relieved.

# References
- Soon Winnie & Cox, Geoff, "Data capture", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 97-119
- “p5.js examples - Interactivity 1,” https://p5js.org/examples/hello-p5-interactivity-1.html.
- “p5.js examples - Interactivity 2,” https://p5js.org/examples/hello-p5-interactivity-2.html.
- “p5 DOM reference,” https://p5js.org/reference/#group-DOM.
- Shoshana Zuboff, “Shoshana Zuboff on Surveillance Capitalism | VPRO Documentary” https://youtu.be/hIXhnWUmMvw.
- Mejias, Ulises A. and Nick Couldry. "Datafication". Internet Policy Review 8.4 (2019). Web. 16 Feb. 2021. https://policyreview.info/concepts/datafication

## Further references
- Soon Winnie: Aesthetic Programming: Class 04

## Truvion

https://kroghsen123.gitlab.io/aestetisk-programmering/Minix7/

![](minix7.PNG)

The game demo is an attempt at recreating the 2D strategy game Travian Kingdoms, which i played a lot when I was younger. You can always watch videos or try the game, in order to get a better idea of what i was going for. For now i've only build a town landscape with construction sites, a moat and some scuffed roads. The primary setup you see when entering the so called game, is all build in the setup function. I put the construction sites into a classed called Cosi, which is located in it's own file. This class contained both visual deciders for the construction sites themselves, but also what would appear on screen if one was to click on a site. The class brings you to a building menu, where you can see the required rescourses needed in order to build different buildings. When one of these are clicked you return to the town. 

If I was better at coding, the intent would be to have the building appear on the Cosi that the played clicked to begin with, but i never figured that one out. This is also why I left in the lines 37-50 in my sketch. These lines show an alternate idea to the push(into array) function I ended up with. The point of giving the Cosis names, was because i was worried the if not specified enough, the building would appear on all of them.

Seeing as the visuals are quite simple and lackluster, I don't feel that there are a lot of implications concerning the abstractions I have made. I hope that it is implicit, due to the selection of buildings you are able to construct, that the area and time is the northern western society in the Middle Ages. The implications of my abstractions could be me deciding which buildings belong in a town, but that feels like a strecth. 

I know I should write more but... God påske :)

## References 
- Soon Winnie & Cox, Geoff, "Object abstraction", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 143-164
- Matthew Fuller and Andrew Goffey, “The Obscure Objects of Object Orientation,” in MatthewFuller, How to be a Geek: Essays on the Culture of Software (Cambridge:Polity, 2017). 
- “p5.js examples - Objects,” https://p5js.org/examples/objects-objects.html.
- “p5.js examples - Array of Objects,” https://p5js.org/examples/objects-array-of-objects.html.
- Daniel Shiffman, “Code! Programming with p5.js,” The Coding Train [Videos 2.3, 6.1, 6.2,6.3, 7.1, 7.2, 7.3], https://www.youtube.com/watch?v=8j0UDiN7my4&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA.

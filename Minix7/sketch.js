let cosi = [];

function setup() {
  createCanvas(800, 800);
  //Moat
  ellipseMode(CENTER);
  fill(50, 50, 255);
  ellipse(400, 405, 330, 210);
//City
  fill(20, 100, 0);
  ellipse(400, 400, 300, 180);
//Roads
  strokeWeight(2);
  stroke(139,69,19);
  line(504, 465, 400, 390);
  line(350, 430, 410, 380);
  line(360, 314, 410, 380);
  line(360, 314, 410, 380);
  line(350, 430, 330, 479);
  line(470, 321, 403, 370);

//construction site
  strokeWeight(1);
  stroke(0);
  cosi.push(new Cosi(440, 455))
  cosi.push(new Cosi(500, 430))
  cosi.push(new Cosi(510, 380))
  cosi.push(new Cosi(475, 345))
  cosi.push(new Cosi(415, 330))
  cosi.push(new Cosi(350, 340))
  cosi.push(new Cosi(300, 375))
  cosi.push(new Cosi(315, 420))
  cosi.push(new Cosi(370, 455))
  cosi.push(new Cosi(370, 380))
  cosi.push(new Cosi(440, 380))
  cosi.push(new Cosi(405, 420))
  // strokeWeight(1);
  // stroke(0);
  // let cosi = new Cosi(440, 455);
  // let cosi1 = new Cosi(500, 430);
  // let cosi2 = new Cosi(510, 380);
  // let cosi3 = new Cosi(475, 345);
  // let cosi4 = new Cosi(415, 330);
  // let cosi5 = new Cosi(350, 340);
  // let cosi6 = new Cosi(300, 375);
  // let cosi7 = new Cosi(315, 420);
  // let cosi8 = new Cosi(370, 455);
  // let cosi9 = new Cosi(370, 380);
  // let cosi10 = new Cosi(440, 380);
  // let cosi11 = new Cosi(405, 420);
}

function mousePressed() {
  for (let i = 0; i < cosi.length; i++) {
     cosi[i].clicked(mouseX, mouseY);
   }
}

function changeSetup() {
  createCanvas(800, 800);
  //Moat
  ellipseMode(CENTER);
  fill(50, 50, 255);
  ellipse(400, 405, 330, 210);
//City
  fill(20, 100, 0);
  ellipse(400, 400, 300, 180);
//Roads
  strokeWeight(2);
  stroke(139,69,19);
  line(504, 465, 400, 390);
  line(350, 430, 410, 380);
  line(360, 314, 410, 380);
  line(360, 314, 410, 380);
  line(350, 430, 330, 479);
  line(470, 321, 403, 370);

//construction site
  strokeWeight(1);
  stroke(0);
  cosi.push(new Cosi(440, 455))
  cosi.push(new Cosi(500, 430))
  cosi.push(new Cosi(510, 380))
  cosi.push(new Cosi(475, 345))
  cosi.push(new Cosi(415, 330))
  cosi.push(new Cosi(350, 340))
  cosi.push(new Cosi(300, 375))
  cosi.push(new Cosi(315, 420))
  cosi.push(new Cosi(370, 455))
  cosi.push(new Cosi(370, 380))
  cosi.push(new Cosi(440, 380))
  cosi.push(new Cosi(405, 420))
}

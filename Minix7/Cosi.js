//construction site class
let button;

class Cosi {
  constructor(x, y) {
  ellipseMode(CENTER);
  this.xpos = x;
  this.ypos = y;
  this.xsize = 40;
  this.ysize = 28;
  ellipse(this.xpos, this.ypos, this.xsize, this.ysize);
  }

  clicked(px, py) {
    let d = dist(px, py, this.xpos, this.ypos);
    if (d < this.xsize && d < this.ysize) {
      background(255);
//Townhall
      textSize(28);
      text("Townhall", 10, 30);
      textSize(12);
      text("Lumber 300", 10, 50);
      text("Stone 250", 10, 65);
      text("Iron 200", 10, 80);
      button = createButton("Build");
      button.position(10, 95);
      button.mousePressed(changeSetup);
// Marketplace
      textSize(28);
      text("Marketplace", 140, 30);
      textSize(12);
      text("Lumber 200", 140, 50);
      text("Stone 200", 140, 65);
      text("Iron 100", 140, 80);
      button = createButton("Build");
      button.position(140, 95);
      button.mousePressed(changeSetup);
// Stable
      textSize(28);
      text("Stable", 310, 30);
      textSize(12);
      text("Lumber 300", 310, 50);
      text("Stone 150", 310, 65);
      text("Iron 150", 310, 80);
      button = createButton("Build");
      button.position(310, 95);
      button.mousePressed(changeSetup);
// Inn
      textSize(28);
      text("Inn", 400, 30);
      textSize(12);
      text("Lumber 300", 400, 50);
      text("Stone 150", 400, 65);
      text("Iron 150", 400, 80);
      button = createButton("Build");
      button.position(400, 95);
      button.mousePressed(changeSetup);
    }
  }
}
